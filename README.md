## Automatisierte Review-Umgebungen mit GitLab-CI und Rancher

#### Projects

Simple HTML: https://gitlab.com/DanielBadura/staging-systems \
Simple Symfony: https://gitlab.com/DanielBadura/symfony-app-staging-systems

#### Talks

| **Conference**                        | **Live** | **Speakerdeck** |
|---------------------------------------|----------|-----------------|
| Symfony User Group Cologne - April'19 | [here](https://sfugcgn-17-04-2019.talk-staging-systems.daniel-badura.de)      | -             |
| Symfony Live Berlin 2019              | [here](https://symfony-live-berlin-2019.talk-staging-systems.daniel-badura.de)      | [here](https://speakerdeck.com/danielbadura/automatisierte-review-umgebungen-mit-gitlab-ci-und-rancher)             |


#### At the moment version

[klick](https://master.talk-staging-systems.daniel-badura.de)
