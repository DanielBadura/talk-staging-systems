FROM node:11-alpine as assets

WORKDIR /var/build
COPY . .

RUN yarn install
RUN yarn build-dev

#####

FROM nginx:1.15

# copy project
COPY --from=assets /var/build/dist/ /usr/share/nginx/html/dist/
COPY index.html /usr/share/nginx/html/index.html
