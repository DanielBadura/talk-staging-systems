// Import React
import React from 'react';
// Import Spectacle Core tags
import { Deck, Fill, Heading, Image, Layout, List, ListItem, Notes, Slide, Text } from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';
import Filename from './Components/Filename';
import Link from './Components/Link';
import Badge from './Components/Badge';

const images = {
  pvlogo: require('../assets/pv-logo-white.svg'),
  pvlogob: require('../assets/pv-logo.svg'),

  profil: require('../assets/profil.jpg'),
  twitter: require('../assets/twitter.svg'),
  gitlab: require('../assets/gitlab.svg'),
  github: require('../assets/github.svg'),

  road: require('../assets/road2.jpg'),
  experience: require('../assets/experience1.jpg'),
  factorio: require('../assets/factorio.png'),

  gitlabci: require('../assets/gitlabci.svg'),
  overviewstep1: require('../assets/overview_step_1.svg'),
  html: require('../assets/html.svg'),
  gitlablogo: require('../assets/gitlab-logo.svg'),

  docker: require('../assets/docker.svg'),
  dockerfile: require('../assets/dockerfile.svg'),
  dockeringitlabciyml: require('../assets/dockeringitlabciyml.svg'),
  overviewstep2: require('../assets/overview_step_2.svg'),
  dockerpipeline: require('../assets/dockerpipeline.png'),

  rancher: require('../assets/cow-blue.svg'),
  rancherui: require('../assets/rancherui.png'),
  rancheringitlabciyml: require('../assets/rancheringitlabciyml.svg'),
  rancherinfrastructure: require('../assets/rancherinfrastructure.svg'),
  dockercompose: require('../assets/docker-compose.svg'),
  overviewstep3: require('../assets/overview_step_3.svg'),
  rancheraccesskey: require('../assets/rancheraccesskey.png'),
  gitlabregistrylogin: require('../assets/gitlabregistrylogin.png'),
  accesskeycivariabel: require('../assets/accesskeycivariabel.png'),
  rancherpipeline: require('../assets/rancherpipeline.png'),
  ranchermr: require('../assets/mergerequestdeploy.png'),
  notfound: require('../assets/404.png'),

  traefik: require('../assets/traefik.svg'),
  traefikui: require('../assets/traefikui.png'),
  traefikincompose: require('../assets/traefikincompose.svg'),
  traefikArchitecture: require('../assets/traefik-architecture.svg'),
  htmlgitlabci: require('../assets/htmlgitlabci.svg'),
  reviewumgebung: require('../assets/reviewumgebung.png'),
  reviewumgebungbranch: require('../assets/reviewumgebungbranch.png'),
  overview: require('../assets/overview.svg'),

  simple: require('../assets/simple.jpg'),
  rocket: require('../assets/rocket.jpg'),

  qanda: require('../assets/qanda.jpg'),

  helloMr: require('../assets/hello-world-mr.png'),
  helloPipeline: require('../assets/hello-world-pipeline.png'),
  helloJob: require('../assets/hello-world-job.png'),

  symfonydockerfile1: require('../assets/symfony-dockerfile-1.svg'),
  symfonydockerfile2: require('../assets/symfony-dockerfile-2.svg'),
  symfonydockerfile3: require('../assets/symfony-dockerfile-3.svg'),
  symfonydockerfilecomplete: require('../assets/symfony-dockerfile-complete.svg'),
  symfonydockercompose: require('../assets/symfony-docker-compose.svg'),
  dockerfolder: require('../assets/dockerfolder.png'),
  bootsh: require('../assets/bootsh.svg'),

  qrcode: require('../assets/qrcode.svg'),
  speakerdeck: require('../assets/speakerdeck.svg')
};

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: '#333333',
    secondary: '#39bbcf',
    tertiary: '#efefef',
    quaternary: '#efefef'
  },
  {
    primary: 'proxima-nova'
  }
);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck theme={theme} progress="number" controls={false} showFullscreenControl={false} contentWidth={'1150px'} contentHeight={'1100px'}>
        <Slide bgColor="primary">
          <Heading textColor="tertiary" textSize={42} padding={5}>Automatisierte</Heading>
          <ContentHeading textColor="secondary" textSize={32} padding={5}>Review-Umgebungen</ContentHeading>
          <Heading textColor="tertiary" textSize={42} padding={5}>mit GitLab-CI und Rancher</Heading>
          <Logo />
        </Slide>
        <Slide bgColor="primary">
          <Layout>
            <Fill>
              <Image src={images.profil} style={{ maxWidth: '70%' }} />
            </Fill>
            <Fill>
              <Heading padding={5} textAlign="left" size={5} textColor="secondary">Daniel Badura</Heading>
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                Software Entwickler<br />bei der pro.volution GmbH
              </Text>
              <br />
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                <FlexedRow>
                  <Link href={'https://twitter.com/danielbadura'}>
                    <MiniLogo src={images.twitter} />
                    @danielbadura
                  </Link>
                </FlexedRow>
              </Text>
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                <FlexedRow>
                  <Link href={'https://github.com/DanielBadura'}>
                    <MiniLogo src={images.github} />
                    DanielBadura
                  </Link>
                </FlexedRow>
              </Text>
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                <FlexedRow>
                  <Link href={'https://gitlab.com/DanielBadura'}>
                    <MiniLogo src={images.gitlab} />
                    DanielBadura
                  </Link>
                </FlexedRow>
              </Text>
            </Fill>
          </Layout>
          <Logo />
          <Notes>
            <ul>
              <li>Daniel Badura</li>
              <li>GitLab wichtig, da alles dort in 3 Repos</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.road} bgDarken={0.5}>
          <ContentHeadingImage>
            Roadmap
          </ContentHeadingImage>
          <Logo />
        </Slide>
        <Slide>
          <RealHeading>
            Roadmap
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Der Ursprung</ListItem>
            <ListItem>Infrastruktur</ListItem>
            <ListItem>Step by Step am Mini-Projekt</ListItem>
            <ListItem>Symfony Beispielprojekt</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Woher kommt das?</li>
              <li>Was brauchen wir dafür?</li>
              <li>Kein Installations-Guide!</li>
              <li>Schritt für schritt Anleitung</li>
              <li>Anpassungen für ein Symfony Beispiel</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.experience} bgDarken={0.5}>
          <ContentHeadingImage>
            Erfahrungen
          </ContentHeadingImage>
          <Logo />
          <Notes>
            <ul>
              <li>Erfahrungen CI? GitLab/Jenkins</li>
              <li>Erfahrungen Docker?</li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            Problem: Abnahme
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>UI Änderungen schwer nachvollziehbar</ListItem>
            <ListItem>Abläufe nicht durch den Code ersichtlich</ListItem>
            <ListItem>Kunde möchte Features abnehmen</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Agentur</li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            Lösung: Staging System
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Deployment auf Staging nach dem Mergen</ListItem>
            <ListItem>Abläufe können getestet werden</ListItem>
            <ListItem>Kunde kann Features abnehmen</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Super, oder?</li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            Problem der Lösung
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Abnahme erst nach dem Mergen</ListItem>
            <ListItem>mehrere Features nicht isoliert testbar</ListItem>
            <ListItem>Feature nicht abgenommen?</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Fast. :D</li>
              <li>Feature nicht abgenommen -> Revert</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.factorio} bgDarken={0.5}>
          <ContentHeadingImage>
            Ziel:<br /> Review-Umgebungen <br />automatisiert erstellen
          </ContentHeadingImage>
          <Logo />
          <Notes>
            <ul>
              <li>Review-Umgebung pro Branch</li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            Was wir dafür brauchen
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>GitLab</ListItem>
            <ListItem>Docker</ListItem>
            <ListItem>Rancher 1.6</ListItem>
            <ListItem>Rancher CLI</ListItem>
            <ListItem>Traefik</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>GitLab und Runner</li>
              <li>Docker und docker-compose</li>
              <li>Rancher 1.6, da wir noch nicht updated haben</li>
              <li>Rancher >2.0 unterstützt nur noch Kubenertes</li>
              <li>Rancher 2.0 leichte Konfig Anpassungen, da Kubernetes</li>
              <li>Traefik als ReverseProxy, SSL</li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            ... das Gute daran
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>
              <div style={{ display: 'inline-flex', alignItems: 'center' }}>
                <span>GitLab</span>
                <Badge>free</Badge>
              </div>
            </ListItem>
            <ListItem>
              <div style={{ display: 'inline-flex', alignItems: 'center' }}>
                <span>Docker</span>
                <Badge>free</Badge>
              </div>
            </ListItem>
            <ListItem>
              <div style={{ display: 'inline-flex', alignItems: 'center' }}>
                <span>Rancher 1.6</span>
                <Badge>free</Badge>
              </div>
            </ListItem>
            <ListItem>
              <div style={{ display: 'inline-flex', alignItems: 'center' }}>
                <span>Rancher CLI</span>
                <Badge>free</Badge>
              </div>
            </ListItem>
            <ListItem>
              <div style={{ display: 'inline-flex', alignItems: 'center' }}>
                <span>Traefik</span>
                <Badge>free</Badge>
              </div>
            </ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Alles was hier vorgestellt wird ist mit den free Versionen erstellt</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Das Projekt: <Filename>index.html</Filename>
          </ImageHeading>
          <Image src={images.html} />
          <LogoB />
          <Notes>
            <ul>
              <li>Simpler geht es nicht</li>
              <li>Next: GitLab</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.gitlablogo} bgRepeat="no-repeat" bgSize="contain" bgDarken={0.5}>
          <ContentHeadingImage>
            GitLab
          </ContentHeadingImage>
          <Logo />
          <Notes>
            <ul>
              <li>Selfhosted oder SaaS</li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            GitLab Features
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>built-in CI/CD</ListItem>
            <ListItem>Docker Registry</ListItem>
            <ListItem>Review Apps</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Menge super Features</li>
              <li>Eingebaute CI/CD</li>
              <li>Eingebaute Docker Registry</li>
              <li>Review-Apps Feature v. GitLab </li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            GitLab-CI
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>config über .gitlab-ci.yml</ListItem>
            <ListItem>sichtbar bei jedem Merge Request</ListItem>
            <ListItem>merge blockierbar durch Pipeline</ListItem>
          </List>
          <Logo />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            <Filename>.gitlab-ci.yml</Filename>
          </ImageHeading>
          <Image src={images.gitlabci} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            GitLab Infrastruktur
          </ImageHeading>
          <Image src={images.overviewstep1} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Merge Request
          </ImageHeading>
          <Image src={images.helloMr} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Pipeline
          </ImageHeading>
          <Image src={images.helloPipeline} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Echo Job
          </ImageHeading>
          <Image src={images.helloJob} />
          <LogoB />
          <Notes>
            <ul>
              <li>Visualisierung von GitLab des Jobs</li>
              <li>Next: Docker</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.docker} bgRepeat="no-repeat" bgSize="contain" bgDarken={0.5}>
          <ContentHeadingImage>
            Docker
          </ContentHeadingImage>
          <Logo />
          <Notes>
            <ul>
              <li>Die meisten kennen Docker ja schon</li>
            </ul>
          </Notes>
        </Slide>
        <Slide>
          <RealHeading>
            Docker
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Image</ListItem>
            <ListItem>Container</ListItem>
            <ListItem>Tags</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Container-Virtualisierung</li>
              <li>Image sozusagen fertiges Paket, Blaupause</li>
              <li>Container aktive Instanz eines Image</li>
              <li>N Container von einem Image startbar</li>
              <li>Tags für Versionierung</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            <Filename>Dockerfile</Filename>
          </ImageHeading>
          <Image src={images.dockerfile} />
          <LogoB />
          <Notes>
            <ul>
              <li>Simples Dockerfile (Image)</li>
              <li>NGINX vererbt</li>
              <li>index.html dort hin verschieben welches nginx standard mäßig ausgibt</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Docker in <Filename>.gitlab-ci.yml</Filename>
          </ImageHeading>
          <Image src={images.dockeringitlabciyml} />
          <LogoB />
          <Notes>
            <ul>
              <li>Image Variabel</li>
              <li>Task Stage weg, Build Stage rein</li>
              <li>Build job docker image</li>
              <li>docker in docker service, für socket binding</li>
              <li>wichtig wenn public runner, sonst runner ggf direkt socket binden</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            GitLab Infrastruktur mit Docker
          </ImageHeading>
          <Image src={images.overviewstep2} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Pipeline mit build Job
          </ImageHeading>
          <Image src={images.dockerpipeline} />
          <LogoB />
          <Notes>
            <ul>
              <li>Next: Rancher</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.rancher} bgRepeat="no-repeat" bgSize="contain" bgDarken={0.5}>
          <ContentHeadingImage>
            Rancher
          </ContentHeadingImage>
          <Logo />
        </Slide>
        <Slide>
          <RealHeading>
            Rancher
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Container Management</ListItem>
            <ListItem>Hostverwaltung</ListItem>
            <ListItem>Web UI</ListItem>
          </List>
          <Logo />
          <Notes>
            <ul>
              <li>Container Management</li>
              <li>mehrere Hosts verwaltbar</li>
              <li>Intuitive UI</li>
              <li>Updates der Stacks mit einem klick</li>
              <li>Rollbacks ebenso</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Rancher Architektur
          </ImageHeading>
          <Image src={images.rancherinfrastructure} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Rancher UI
          </ImageHeading>
          <Image src={images.rancherui} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Rancher in <Filename>.gitlab-ci.yml</Filename>
          </ImageHeading>
          <Image src={images.rancheringitlabciyml} />
          <LogoB />
          <Notes>
            <ul>
              <li>Domain Variabel -> Traefik</li>
              <li>Stack Variabel -> Rancher</li>
              <li>Rancher CLI Download</li>
              <li>Rancher up: mehr output, prune container etc.</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            <Filename>docker-compose.yml</Filename>
          </ImageHeading>
          <Image src={images.dockercompose} />
          <LogoB />
          <Notes>
            <ul>
              <li>docker-compose.yml für Rancher</li>
              <li>bildet unseren Stack in Rancher ab</li>
              <li>gibt an wie unsere Container hochgefahren werden sollen</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            GitLab Infrastruktur mit Docker & Rancher
          </ImageHeading>
          <Image src={images.overviewstep3} />
          <LogoB />
        </Slide>
        {/*
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Rancher-API Access-Key
          </ImageHeading>
          <Image src={images.rancheraccesskey} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Rancher Zugriff auf GitLab Docker Registry
          </ImageHeading>
          <Image src={images.gitlabregistrylogin} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            CI Variabeln für Rancher API
          </ImageHeading>
          <Image src={images.accesskeycivariabel} />
          <LogoB />
        </Slide>
        */}
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Pipeline mit review Job
          </ImageHeading>
          <Image src={images.rancherpipeline} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Merge Request
          </ImageHeading>
          <Image src={images.ranchermr} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Not Found
          </ImageHeading>
          <Image src={images.notfound} />
          <LogoB />
          <Notes>
            <ul>
              <li>kein hit, 404</li>
              <li>Next: Traefik</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.traefik} bgRepeat="no-repeat" bgSize="contain" bgDarken={0.5}>
          <ContentHeadingImage>
            Traefik
          </ContentHeadingImage>
          <Logo />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Traefik Architektur
          </ImageHeading>
          <Image src={images.traefikArchitecture} />
          <LogoB />
          <Notes>
            <ul>
              <li>Reverse proxy</li>
              <li>Loadbalancer</li>
              <li>Rancher Integration</li>
              <li>Dynamische Host Auflösung auf Container</li>
              <li>LetsEncrypt Integration</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Traefik UI
          </ImageHeading>
          <Image src={images.traefikui} />
          <LogoB />
          <Notes>
            <ul>
              <li>leider nicht so viel zu sehen</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Traefik in <Filename>docker-compose.yml</Filename>
          </ImageHeading>
          <Image src={images.traefikincompose} />
          <LogoB />
          <Notes>
            <ul>
              <li>Labels</li>
              <li>frontend.rule = routing</li>
              <li>domain = ssl</li>
              <li>port = auf welchen port zum container</li>
              <li>enable = traefik beachte den container</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Übersicht
          </ImageHeading>
          <Image src={images.overview} />
          <LogoB />
          <Notes>
            <ul>
              <li>Komplette Übersicht</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            komplette <Filename>.gitlab-ci.yml</Filename>
          </ImageHeading>
          <Image src={images.htmlgitlabci} />
          <LogoB />
          <Notes>
            <ul>
              <li>Komplette Übersicht ci yml</li>
              <li>Wahrscheinlich nicht lesbar</li>
              <li>46 Zeilen</li>
              <li>Next: Ansehen</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Review-Umgebung
          </ImageHeading>
          <Image src={images.reviewumgebung} />
          <LogoB />
          <Notes>
            <ul>
              <li>Master</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            Review-Umgebung Branch
          </ImageHeading>
          <Image src={images.reviewumgebungbranch} />
          <LogoB />
          <Notes>
            <ul>
              <li>Branch</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.simple} bgDarken={0.5}>
          <ContentHeadingImage>
            Simple as that
          </ContentHeadingImage>
          <Logo />
          <Notes>
            <ul>
              <li>3 Files im Projekt</li>
              <li>Next Anpassungen für Symfony</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            "Symfony" <Filename>Dockerfile</Filename> 1/3
          </ImageHeading>
          <Image src={images.symfonydockerfile1} />
          <LogoB />
          <Notes>
            <ul>
              <li>Single Docker Image</li>
              <li>Einfachheithalber</li>
              <li>Gute Docker tutorials da draußen</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            "Symfony" <Filename>Dockerfile</Filename> 2/3
          </ImageHeading>
          <Image src={images.symfonydockerfile2} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            "Symfony" <Filename>Dockerfile</Filename> 3/3
          </ImageHeading>
          <Image src={images.symfonydockerfile3} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            "Symfony" <Filename>docker-compose.yml</Filename>
          </ImageHeading>
          <Image src={images.symfonydockercompose} />
          <LogoB />
          <Notes>
            <ul>
              <li>App ENVs</li>
              <li>Datenbank dran linken, mysql</li>
              <li>Hostsystem sollte genügend Asynchcrone I/O Sockets offen haben</li>
              <li>Das wars auch schon an Änderungen</li>
            </ul>
          </Notes>
        </Slide>
        {/*
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            App Docker Config Ordner
          </ImageHeading>
          <Image src={images.dockerfolder} />
          <LogoB />
        </Slide>
        <Slide bgColor="tertiary" progressColor="#888" controlColor="#888">
          <ImageHeading>
            <Filename>boot.sh</Filename>
          </ImageHeading>
          <Image src={images.bootsh} />
          <LogoB />
        </Slide>
        */}
        <Slide bgImage={images.rocket} bgDarken={0.5}>
          <ContentHeadingImage>
            Review-Umgebung bauen
          </ContentHeadingImage>
          <ContentHeadingImage>
            Semi-Live!
          </ContentHeadingImage>
          <Logo />
          <Notes>
            <ul>
              <li>Kleines Video vorbereitet</li>
              <li>11 Minuten hat die ganze Pipeline gedauert</li>
            </ul>
          </Notes>
        </Slide>
        <Slide bgImage={images.qanda} bgDarken={0.5}>
          <ContentHeadingImage>
            Q&A
          </ContentHeadingImage>
          <Logo />
        </Slide>
        <Slide style={{ maxWidth: '1100px' }}>
          <ContentHeading>
            Vielen Dank!
          </ContentHeading>
          <br/>
          <Layout style={{ alignItems: 'center' }}>
            <Image src={images.qrcode} height={250} style={{ margin: '0 2rem 0 0' }} />
            <div
              style={{
                textAlign: 'left',
                fontSize: '1.5rem'
              }}
            >
              <FlexedRow style={{ margin: '0.6rem' }}>
                <Link href={'https://symfony-live-berlin-2019.talk-staging-systems.daniel-badura.de'}>
                  <MiniLogo src={images.gitlab} />
                  https://symfony-live-berlin-2019.talk-staging-systems.daniel-badura.de
                </Link>
              </FlexedRow>
              <FlexedRow style={{ margin: '0.6rem' }}>
                <Link href={'https://speakerdeck.com/danielbadura/automatisierte-review-umgebungen-mit-gitlab-ci-und-rancher'}>
                  <MiniLogo src={images.speakerdeck} />
                  https://speakerdeck.com
                </Link>
              </FlexedRow>
              <hr style={{ borderColor: theme.screen.colors.tertiary, marginTop: '30px', marginBottom: '30px', opacity: '0.5' }}/>
              <FlexedRow style={{ margin: '0.6rem' }}>
                <Link href={'https://gitlab.com/DanielBadura/talk-staging-systems'}>
                  <MiniLogo src={images.gitlab} />
                  https://gitlab.com/DanielBadura/talk-staging-systems
                </Link>
              </FlexedRow>
              <FlexedRow style={{ margin: '0.6rem' }}>
                <Link href={'https://gitlab.com/DanielBadura/staging-systems'}>
                  <MiniLogo src={images.gitlab} />
                  https://gitlab.com/DanielBadura/staging-systems
                </Link>
              </FlexedRow>
              <FlexedRow style={{ margin: '0.6rem' }}>
                <Link href={'https://gitlab.com/DanielBadura/symfony-app-staging-systems'}>
                  <MiniLogo src={images.gitlab} />
                  https://gitlab.com/DanielBadura/symfony-app-staging-systems
                </Link>
              </FlexedRow>
            </div>
          </Layout>
          <Logo />
        </Slide>
      </Deck>
    );
  }
}

class Logo extends React.Component {
  render() {
    const style = {
      width: 200,
      bottom: 10,
      left: 10,
      position: 'absolute'
    };

    return (
      <img style={style} src={images.pvlogo} />
    );
  }
}

class LogoB extends React.Component {
  render() {
    const style = {
      width: 200,
      bottom: 10,
      left: 10,
      position: 'absolute'
    };

    return (
      <img style={style} src={images.pvlogob} />
    );
  }
}

const RealHeading = ({ children }) => (
  <Heading size={3} lineHeight={1} textColor="secondary" textAlign="left">
    {children}
  </Heading>
);

class ContentHeadingImage extends React.Component {
  render() {
    const { children } = this.props;

    const background = {
      width: '150%',
      marginLeft: '-25%'
    };

    const text = {};

    return (
      <div style={background}>
        <Heading size={3} lineHeight={1} textColor="tertiary" style={text}>
          {children}
        </Heading>
      </div>
    );
  }
}

const ContentHeading = ({ children }) => (
  <Heading size={3} lineHeight={1} textColor="secondary">
    {children}
  </Heading>
);

class MiniLogo extends React.Component {
  render() {
    const { src } = this.props;

    const style = {
      margin: 0,
      marginRight: 10,
      display: 'inline'
    };

    return (
      <Image src={src} width={32} style={style} />
    );
  }
}

const FlexedRow = ({ children, style }) => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center',
      ...style
    }}
  >
    {children}
  </div>
);

const ImageHeading = ({ children }) => (
  <span
    style={{
      color: '#888',
      textAlign: 'left',
      fontWeight: 'bold'
    }}
    className="test"
    id="test"
  >
    {children}
  </span>
);

