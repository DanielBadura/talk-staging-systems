import React from 'react';
import './Filename.css';

const Filename = ({ children, ...props }) => (
  <code className="Filename" {...props}>
    {children}
  </code>
);

export default Filename;
