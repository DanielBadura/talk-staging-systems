import React from 'react';
import './Badge.css';

const Badge = ({ children, ...props }) => (
  <span className="Badge" {...props}>{children}</span>
);

export default Badge;
